$ ->
  desc_sec = $('.section__description')
  you_sec = $('.section__youtube')
  rev_sec = $('.section__reviews')
  soc_sec = $('.section__social')
  price_sec = $('.section__price')


  desc_button = $('.link__description')
  you_button = $('.link__youtube')
  soc_button = $('.link__social')
  rev_button = $('.link__reviews')
  price_button = $('.link__price')


  #Description button
  desc_button.click ->
    $(@).addClass('link_is_active')

    you_button.removeClass('link_is_active')
    soc_button.removeClass('link_is_active')
    rev_button.removeClass('link_is_active')
    price_button.removeClass('link_is_active')

    desc_sec.show()
    you_sec.hide()
    soc_sec.hide()
    rev_sec.hide()
    price_sec.hide()

  desc_button.click (e) ->
    e.preventDefault()
    return

  # Social Button
  soc_button.click ->
    $(@).addClass('link_is_active')

    you_button.removeClass('link_is_active')
    desc_button.removeClass('link_is_active')
    rev_button.removeClass('link_is_active')
    price_button.removeClass('link_is_active')

    soc_sec.show()
    you_sec.hide()
    desc_sec.hide()
    rev_sec.hide()
    price_sec.hide()

  soc_button.click (e) ->
    e.preventDefault()
    return

  # Reviews button
  rev_button.click ->
    $(@).addClass('link_is_active')

    you_button.removeClass('link_is_active')
    desc_button.removeClass('link_is_active')
    soc_button.removeClass('link_is_active')
    price_button.removeClass('link_is_active')

    rev_sec.show()
    you_sec.hide()
    soc_sec.hide()
    desc_sec.hide()
    price_sec.hide()

  rev_button.click (e) ->
    e.preventDefault()
    return

  # Youtube button
  you_button.click ->
    $(@).addClass('link_is_active')

    soc_button.removeClass('link_is_active')
    desc_button.removeClass('link_is_active')
    rev_button.removeClass('link_is_active')
    price_button.removeClass('link_is_active')

    you_sec.show()
    desc_sec.hide()
    soc_sec.hide()
    rev_sec.hide()
    price_sec.hide()

  you_button.click (e) ->
    e.preventDefault()
    return

  # Price button
  price_button.click ->
    $(@).addClass('link_is_active')

    soc_button.removeClass('link_is_active')
    desc_button.removeClass('link_is_active')
    rev_button.removeClass('link_is_active')
    you_button.removeClass('link_is_active')

    price_sec.show()
    desc_sec.hide()
    soc_sec.hide()
    rev_sec.hide()
    you_sec.hide()

  you_button.click (e) ->
    e.preventDefault()
    return

#Inner Tabs
  tw_but = $('.soc_tab__tw')
  gl_but = $('.soc_tab__gl')

  tw_sec = $('.soc_tabs_twitter')
  gl_sec = $('.soc_tabs_google')

  tw_but.click (e) ->
    e.preventDefault()
    return

  gl_but.click (e) ->
    e.preventDefault()
    return

  tw_but.click ->
    $(@).addClass('social_is_active')
    gl_but.removeClass('social_is_active')
    tw_sec.show()
    gl_sec.hide()

  gl_but.click ->
    $(@).addClass('social_is_active')
    tw_but.removeClass('social_is_active')
    gl_sec.show()
    tw_sec.hide()
