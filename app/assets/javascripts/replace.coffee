$ ->

  button = $('.js_details_button')

  button.click ->
    parent_block = $(@).parent().parent().parent()
    appended_block = $('#item__id_1')

    #title
    link_title = parent_block.children('.item_header').children('.item_header__link').text()
    link_href = parent_block.children('.item_header').children('.item_header__link').attr('href')
    appended_block.children('.item_header').children('.item_header__link').text(link_title)
    appended_block.children('.item_header').children('.item_header__link').attr('href', link_href)

    #price
    price = parent_block.children('.item_body').children('.show_for_side').children('.side_price').html()
    appended_block.children('.item_header').children('.item_header__price').html(price)

    #image
    image = parent_block.children('.item_body').children('.image__container').children('.item_body__image').attr('src')
    appended_block.children('.item_body').children('.image__container').children('.item_body__image').attr('src', image)
    console.log image

    #social && youtube
    social_href = parent_block.children('.item_body').children('.tabs').children('.tabs__links').children('.link__social').attr('href')
    appended_block.children('.item_body').children('.tabs').children('.tabs__links').children('.link__social').attr('href', social_href)
    appended_block.children('.item_body').children('.tabs').children('.tabs__links').children('.link__youtube').attr('href', social_href)
    appended_block.children('.item_body').children('.tabs').children('.tabs__links').children('.link__reviews').attr('href', social_href)

    #description
    section = parent_block.children('.item_body').children('.tabs').children('.tabs__sections').children('.section__description').html()
    appended_block.children('.item_body').children('.tabs').children('.tabs__sections').children('.section__description').html(section)

    #price
    price = parent_block.children('.item_body').children('.tabs').children('.tabs__sections').children('.section__price').html()
    appended_block.children('.item_body').children('.tabs').children('.tabs__sections').children('.section__price').html(price)
