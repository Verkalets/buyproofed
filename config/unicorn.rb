working_directory "/home/deploy/buyproofed.com/current"

pid "/home/deploy/buyproofed.com/shared/pids/unicorn.pid"

stderr_path "/home/deploy/buyproofed.com/shared/log/unicorn.log"
stdout_path "/home/deploy/buyproofed.com/shared/log/unicorn.log"

listen "/tmp/unicorn.buyproofed.sock"

worker_processes 2

timeout 30
